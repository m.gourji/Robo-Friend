import React from 'react';

const Scroll = (props) => {
  return (
    <div style={{ overflow: 'scroll', border: 'grab(0.0.0.0)', height: '800px'}}>
      {props.children}
    </div>
  );
}

export default Scroll;
